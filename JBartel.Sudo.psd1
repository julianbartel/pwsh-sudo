@{
    RootModule = 'JBartel.Sudo.psm1'
    ModuleVersion = '1.0.0'
    Author = 'Julian Bartel'
    Description = 'Invokes commands as administrator'
    GUID = 'e3c0fe02-bd11-4a06-b3e6-4debb48756e0'
    PowerShellVersion = '5.1'
}