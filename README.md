# PowerShell sudo

## Installation

```powershell
Install-Module JBartel.Sudo
```

## Usage

```powershell
Invoke-AsAdmin { Write-Host "This runs in an elevated pwsh"; pause; }
# or use alias sudo:
sudo { Write-Host "This runs in an elevated pwsh"; pause; }
```

## Limitations

Due to Windows UAC, a new PWSH instance is started and stdin/stderr/stdout is not transported to the original instance.