function Invoke-AsAdmin
{
    [CmdletBinding()]
    param (
        [Parameter(ValueFromRemainingArguments = $true, Mandatory = $true, ParameterSetName = "CommandList", Position = 0)][string[]] $Commands,
        [Parameter(Mandatory = $true, ParameterSetName = "ScriptBlock", Position = 0)][scriptblock] $ScriptBlock,
        [Parameter(ParameterSetName = "ScriptBlock", Position = 1)][string[]] $ArgumentList
    )

    $currentLocation = Get-Location

    if ($null -ne $Commands) {
        $command = [string]::Join(' ', $Commands);
    } else {
        if ($null -eq $ArgumentList) {
            $command = "Set-Location $currentLocation; Invoke-Command { $ScriptBlock }"
        } else {             
            $command = "Set-Location $currentLocation; Invoke-Command { $ScriptBlock } -ArgumentList $ArgumentList"
        }
    }
    
    $command | Write-Verbose
    $bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
    $encodedCommand = [Convert]::ToBase64String($bytes)
    Start-Process pwsh @( "-e", $encodedCommand, "--NoLogo") -Verb RunAs -Wait
}

New-Alias sudo Invoke-AsAdmin

Export-ModuleMember `
    -Function Invoke-AsAdmin `
    -Alias sudo